import unittest
from unittest.mock import patch, call

from trivia.trivia import Game


class RollTest(unittest.TestCase):
    @patch('builtins.print')
    def test_roll_1_with_two_players(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')

        # when
        game.roll(1)

        # then
        print_mock.assert_has_calls(
            [
                call('Arthur is the current player'),
                call('They have rolled a 1'),
                call("Arthur's new location is 1"),
                call('The category is Science'),
                call('Science Question 0')
            ]
        )

    @patch('builtins.print')
    def test_roll_2_with_two_players(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')

        # when
        game.roll(2)

        # then
        print_mock.assert_has_calls(
            [
                call('Arthur is the current player'),
                call('They have rolled a 2'),
                call("Arthur's new location is 2"),
                call('The category is Sports'),
                call('Sports Question 0')
            ]
        )

    @patch('builtins.print')
    def test_roll_3_with_two_players(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')

        # when
        game.roll(3)

        # then
        print_mock.assert_has_calls(
            [
                call('Arthur is the current player'),
                call('They have rolled a 3'),
                call("Arthur's new location is 3"),
                call('The category is Rock'),
                call('Rock Question 0')
            ]
        )

    @patch('builtins.print')
    def test_roll_4_with_two_players(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')

        # when
        game.roll(4)

        # then
        print_mock.assert_has_calls(
            [
                call('Arthur is the current player'),
                call('They have rolled a 4'),
                call("Arthur's new location is 4"),
                call('The category is Pop'),
                call('Pop Question 0')
            ]
        )

    @patch('builtins.print')
    def test_roll_5_with_two_players(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')

        # when
        game.roll(5)

        # then
        print_mock.assert_has_calls(
            [
                call('Arthur is the current player'),
                call('They have rolled a 5'),
                call("Arthur's new location is 5"),
                call('The category is Science'),
                call('Science Question 0')
            ]
        )

    @patch('builtins.print')
    def test_roll_6_with_two_players(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')

        # when
        game.roll(6)

        # then
        print_mock.assert_has_calls(
            [
                call('Arthur is the current player'),
                call('They have rolled a 6'),
                call("Arthur's new location is 6"),
                call('The category is Sports'),
                call('Sports Question 0')
            ]
        )

    def test_roll_with_current_player_out_of_penalty_box_should_move_player_with_rolled_distance(self):
        # given
        game = Game()
        game.add('Arthur')
        game.places[0] = 0

        # when
        game.roll(3)

        # then
        assert game.places[0] == 3

    def test_roll_with_current_player_out_of_penalty_box_should_move_player_circularly_after_place_11_with_rolled_distance(
            self):
        # given
        game = Game()
        game.add('Arthur')
        game.places[0] = 11

        # when
        game.roll(3)

        # then
        assert game.places[0] == 2

    def test_roll_odd_with_current_player_in_penalty_box_should_make_player_get_out(self):
        # given
        game = Game()
        game.add('Arthur')
        game.in_penalty_box[0] = True

        # when
        game.roll(1)

        # then
        assert game.is_getting_out_of_penalty_box is True

    @patch("builtins.print")
    def test_roll_odd_with_current_player_in_penalty_box_should_print_player_is_getting_out(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.in_penalty_box[0] = True

        # when
        game.roll(1)

        # then
        print_mock.assert_has_calls([
            call('Arthur is getting out of the penalty box')
        ])

    @patch("builtins.print")
    def test_roll_odd_with_current_player_in_penalty_box_should_move_player_and_ask_question(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.in_penalty_box[0] = True
        game.places[0] = 0

        # when
        game.roll(1)

        # then
        assert game.places[0] == 1
        print_mock.assert_has_calls([
            call("Arthur's new location is 1"),
            call('The category is Science'),
            call('Science Question 0')
        ])

    def test_roll_odd_with_current_player_in_penalty_box_should_move_player_circularly_after_place_eleven(self):
        # given
        game = Game()
        game.add('Arthur')
        game.in_penalty_box[0] = True
        game.places[0] = 11

        # when
        game.roll(3)

        # then
        assert game.places[0] == 2

    @patch("builtins.print")
    def test_roll_even_with_current_player_in_penalty_box_should_leave_player_in_penalty_box(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.in_penalty_box[0] = True
        game.places[0] = 0

        # when
        game.roll(2)

        # then
        assert game.places[0] == 0
        assert game.is_getting_out_of_penalty_box is False
        print_mock.assert_has_calls([
            call('Arthur is not getting out of the penalty box')
        ])

    @patch("builtins.print")
    def test_roll_ask_sports_question_if_place_is_2_6_or_10(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')

        for dice in [2, 6, 10]:
            game.places[0] = dice - 1

            # when
            game.roll(1)

            # then
            print_mock.assert_has_calls([call('Sports Question 0')])

    @patch("builtins.print")
    def test_roll_ask_pop_question_if_place_is_0_4_or_8(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')

        for dice in [0, 4, 8]:
            game.places[0] = dice - 1 if dice > 0 else 11

            # when
            game.roll(1)

            # then
            print_mock.assert_has_calls([call('Pop Question 0')])

    @patch("builtins.print")
    def test_roll_ask_science_question_if_place_is_1_5_or_9(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')

        for dice in [1, 5, 9]:
            game.places[0] = dice - 1

            # when
            game.roll(1)

            # then
            print_mock.assert_has_calls([call('Science Question 0')])

    @patch("builtins.print")
    def test_roll_ask_rock_question_if_place_is_3_7_or_11(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')

        for dice in [3, 7, 11]:
            game.places[0] = dice - 1

            # when
            game.roll(1)

            # then
            print_mock.assert_has_calls([call('Rock Question 0')])
