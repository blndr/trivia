import unittest
from unittest.mock import patch, call

from trivia.trivia import Game


class AddTest(unittest.TestCase):
    @patch('builtins.print')
    def test_add_player_should_print_messages(self, print_mock):
        # given
        game = Game()

        # when
        game.add('Arthur')

        # then
        print_mock.assert_has_calls(
            [
                call('Arthur was added'),
                call('They are player number 1'),
            ]
        )

    def test_add_player_init_place_and_purse_and_in_penalty_box_of_next_player(self):
        # given
        game = Game()

        # when
        game.add('Arthur')

        # then
        assert game.places[1] == 0
        assert game.purses[1] == 0
        assert game.in_penalty_box[1] is False

    def test_add_player_should_add_player_to_list(self):
        # given
        game = Game()

        # when
        game.add('Arthur')

        # then
        assert game.places[1] == 0
        assert game.purses[1] == 0
        assert 'Arthur' in game.players
