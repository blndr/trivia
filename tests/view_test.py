import unittest
from unittest.mock import patch, call

from trivia.view import print_next_place, print_wrong_answer


@patch('builtins.print')
class ViewTest(unittest.TestCase):
    def test_print_next_place(self, print_mock):
        # when
        print_next_place('bob', 1, 'rock')

        # then
        print_mock.assert_has_calls(
            [
                call("bob's new location is 1"),
                call("The category is rock")
            ]
        )

    def test_print_wrong_answer(self, print_mock):
        # when
        print_wrong_answer("bob")

        # then
        print_mock.assert_has_calls(
            [
                call("Question was incorrectly answered"),
                call("bob was sent to the penalty box")
            ]
        )