import unittest
from unittest.mock import patch, call

from trivia.trivia import Game


class HandleCorrectAnswerTest(unittest.TestCase):
    @patch('builtins.print')
    def test_should_print_correct_answer_and_gold(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')

        # when
        game.handle_correct_answer()

        # then
        print_mock.assert_has_calls(
            [
                call('They are player number 1'),
                call('Answer was correct!!!!'),
                call('Arthur now has 1 Gold Coins.')
            ]
        )

    def test_should_increment_current_player(self):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')
        current_player = game.current_player

        # when
        game.handle_correct_answer()

        # then
        assert game.current_player == current_player + 1

    def test_should_increment_current_player_golds(self):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')
        game.current_player = 0
        game.purses[0] = 1
        game.purses[1] = 0

        # when
        game.handle_correct_answer()

        # then
        assert game.purses[0] == 2

    def test_should_return_false_when_player_gold_is_6(self):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')
        game.current_player = 0
        game.purses[0] = 5
        game.purses[1] = 0

        # when
        result = game.handle_correct_answer()

        # then
        assert result is False

    @patch('builtins.print')
    def test_should_print_something_after_current_player_gave_wrong_answer_but_is_getting_out_of_penalty_box(
            self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.in_penalty_box[0] = True
        game.is_getting_out_of_penalty_box = True

        # when
        game.handle_correct_answer()

        # then
        print_mock.assert_has_calls(
            [
                call('Answer was correct!!!!'),
                call('Arthur now has 1 Gold Coins.')

            ]
        )

    def test_should_increment_current_player_after_current_player_gave_wrong_answer_but_is_getting_out_of_penalty_box(
            self):
        # given
        game = Game()
        game.add('Abel')
        game.add('Arthur')
        current_player = game.current_player
        game.in_penalty_box[0] = True
        game.is_getting_out_of_penalty_box = True

        # when
        game.handle_correct_answer()

        # then
        assert game.current_player == current_player + 1

    def test_should_increment_current_player_golds_after_current_player_gave_wrong_answer_but_is_getting_out_of_penalty_box(
            self):
        # given
        game = Game()
        game.add('Abel')
        game.add('Arthur')
        game.purses[0] = 1
        game.current_player = 0
        game.in_penalty_box[0] = True
        game.is_getting_out_of_penalty_box = True

        # when
        game.handle_correct_answer()

        # then
        assert game.purses[0] == 2

    def test_should_increment_current_player_after_current_player_gave_wrong_answer_but_is_not_getting_out_of_penalty_box(
            self):
        # given
        game = Game()
        game.add('Abel')
        game.add('Arthur')
        game.in_penalty_box[0] = True
        game.is_getting_out_of_penalty_box = False
        current_player = game.current_player

        # when
        result = game.handle_correct_answer()

        # then
        assert game.current_player == current_player + 1
        assert result is True

    def test_should_not_increment_current_player_golds_after_current_player_gave_wrong_answer_but_is_not_getting_out_of_penalty_box(
            self):
        # given
        game = Game()
        game.add('Abel')
        game.add('Arthur')
        game.current_player = 0
        game.purses[0] = 1
        game.in_penalty_box[0] = True
        game.is_getting_out_of_penalty_box = False

        # when
        game.handle_correct_answer()

        # then
        assert game.purses[0] == 1
