import unittest
from unittest.mock import patch, call

from trivia.trivia import Game


class HandleWrongAnswerTest(unittest.TestCase):
    def test_should_return_true(self):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')

        # when
        wrong_answer = game.handle_wrong_answer()

        # then
        assert wrong_answer

    @patch('builtins.print')
    def test_should_print_penalty_box_message(self, print_mock):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')

        # when
        game.handle_wrong_answer()

        # then
        print_mock.assert_has_calls(
            [
                call('Question was incorrectly answered'),
                call('Arthur was sent to the penalty box')
            ]
        )

    def test_should_put_current_player_in_penalty_box(self):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')
        current_player = game.current_player

        # when
        game.handle_wrong_answer()

        # then
        assert game.in_penalty_box[current_player] is True

    def test_should_increment_current_player(self):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')
        previous_player = game.current_player

        # when
        game.handle_wrong_answer()

        # then
        assert game.current_player == previous_player + 1

    def test_should_go_back_to_first_player_when_incrementing_current_player(self):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')
        game.handle_wrong_answer()

        # when
        game.handle_wrong_answer()

        # then
        assert game.current_player == 0
