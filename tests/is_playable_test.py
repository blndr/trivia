import unittest

from trivia.trivia import Game


class IsPlayableTest(unittest.TestCase):
    def test_is_playable_returns_false_if_no_players(self):
        # given
        game = Game()

        # when
        playable = game.is_playable()

        # then
        assert playable is False

    def test_is_playable_returns_false_if_one_player(self):
        # given
        game = Game()
        game.add('Arthur')

        # when
        playable = game.is_playable()

        # then
        assert playable is False

    def test_is_playable_returns_true_if_two_players(self):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')

        # when
        playable = game.is_playable()

        # then
        assert playable is True

    def test_is_playable_returns_true_if_more_than_two_players(self):
        # given
        game = Game()
        game.add('Arthur')
        game.add('Abel')
        game.add('Laurent')

        # when
        playable = game.is_playable()

        # then
        assert playable is True
