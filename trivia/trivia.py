class Game:
    def __init__(self):
        self.BOARD_SIZE = 12
        self.MAX_PLAYERS = 6

        categories = ['Pop', 'Science', 'Sports', 'Rock']
        self.board = categories * (self.BOARD_SIZE // len(categories))
        self.players = []
        self.places = [0] * self.MAX_PLAYERS
        self.purses = [0] * self.MAX_PLAYERS

        self.in_penalty_box = [0] * self.MAX_PLAYERS
        self.current_player = 0
        self.is_getting_out_of_penalty_box = False
        self._init_questions(categories)

    def is_playable(self):
        return self.how_many_players >= 2

    def add(self, player_name):
        self.players.append(player_name)
        self.places[self.how_many_players] = 0
        self.purses[self.how_many_players] = 0
        self.in_penalty_box[self.how_many_players] = False

        self._print_new_player(player_name)

    def roll(self, roll):
        self._print_current_move(roll)

        if self._current_player_in_penalty_box():
            getting_out = self._get_out_of_penalty_box(roll)
            self.is_getting_out_of_penalty_box = getting_out
            self._print_penalty_box(getting_out)

        if not self._current_player_in_penalty_box() or self.is_getting_out_of_penalty_box:
            self._play_turn(roll)

    def handle_correct_answer(self):
        continue_playing = True

        if not self._current_player_in_penalty_box() or self.is_getting_out_of_penalty_box:
            self._print_correct_answer()
            self.purses[self.current_player] += 1
            self._print_purse()
            continue_playing = not self._winner()

        self._set_next_player()
        return continue_playing

    def handle_wrong_answer(self):
        continue_playing = True
        self._print_wrong_answer()
        self.in_penalty_box[self.current_player] = True
        self._set_next_player()
        return continue_playing

    def _current_player_in_penalty_box(self):
        return self.in_penalty_box[self.current_player]

    def _current_player_name(self):
        return self.players[self.current_player]

    def _play_turn(self, roll):
        self._move_player(roll)
        self._print_next_place()
        self._ask_question()

    def _winner(self):
        return self.purses[self.current_player] == 6

    def _create_question(self, category, index):
        return '%s Question %s' % (category, index)

    def _set_next_player(self):
        self.current_player += 1
        if self.current_player == len(self.players):
            self.current_player = 0

    def _ask_question(self):
        print(self.questions[self.current_category].pop(0))

    def _move_player(self, roll):
        self.places[self.current_player] = (self._current_player_place() + roll) % self.BOARD_SIZE

    def _current_player_place(self):
        return self.places[self.current_player]

    def _init_questions(self, categories):
        self.questions = {}
        for category in categories:
            self.questions[category] = []
            for i in range(50):
                self.questions[category].append(self._create_question(category, i))

    def _get_out_of_penalty_box(self, roll):
        return roll % 2 != 0

    def _print_correct_answer(self):
        print('Answer was correct!!!!')

    def _print_next_place(self):
        print(self._current_player_name() + '\'s new location is ' + str(self._current_player_place()))
        print('The category is %s' % self.current_category)

    def _print_wrong_answer(self):
        print('Question was incorrectly answered')
        print('%s was sent to the penalty box' % self._current_player_name())

    def _print_penalty_box(self, getting_out):
        if getting_out:
            print('%s is getting out of the penalty box' % self._current_player_name())
        else:
            print('%s is not getting out of the penalty box' % self._current_player_name())

    def _print_current_move(self, roll):
        print('%s is the current player' % self._current_player_name())
        print('They have rolled a %s' % roll)

    def _print_purse(self):
        print(self._current_player_name() + ' now has ' + str(self.purses[self.current_player]) + ' Gold Coins.')

    def _print_new_player(self, player_name):
        print('%s was added' % player_name)
        print('They are player number %s' % len(self.players))

    @property
    def how_many_players(self):
        return len(self.players)

    @property
    def current_category(self):
        place = self._current_player_place()
        return self.board[place]
