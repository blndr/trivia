import unittest

from trivia.trivia import Game


class GameTest(unittest.TestCase):
    def test_game_constructor_should_init_game_with_50_questions_by_category(self):
        # when
        game = Game()

        # then
        assert len(game.questions['Pop']) == 50
        assert len(game.questions['Science']) == 50
        assert len(game.questions['Rock']) == 50
        assert len(game.questions['Sports']) == 50

    def test_game_constructor_should_init_game_with_current_player(self):
        # when
        game = Game()

        # then
        assert game.current_player == 0
        assert not game.is_getting_out_of_penalty_box

    def test_game_constructor_should_init_game_with_no_players_and_6_places_and_purses_and_penalty_boxes(self):
        # when
        game = Game()

        # then
        assert len(game.players) == 0
        assert len(game.places) == 6
        assert len(game.purses) == 6
        assert len(game.in_penalty_box) == 6

    def test_game_constructor_should_init_game_with_questions(self):
        # when
        game = Game()

        # then
        for i in range(50):
            assert game.questions['Pop'][i] == ("Pop Question %s" % i)
            assert game.questions['Science'][i] == ("Science Question %s" % i)
            assert game.questions['Sports'][i] == "Sports Question %s" % i
            assert game.questions['Rock'][i] == "Rock Question %s" % i
