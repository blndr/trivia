# Trivia
This is a project kata found here (https://github.com/jbrains/trivia).
Its purpose is to refacto a poorly written, untested, possibly bugged legacy code without any documentation to read beforehand.

## Execution
To have an example of what the code does :
```
. venv/bin/activate
python runner.py
```

## Requirements
Python 3.x is required.
Please setup your virtualenv with Python 3 and install the requirements using the ```requirements.txt``` file provided.
```
. venv/bin/activate
pip install -r requirements.txt
```

## Unit test execution
```
. venv/bin/activate
pytest
```
